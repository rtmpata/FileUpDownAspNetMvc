﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileUpDownLoad.Models
{
    public enum FileType
    {
        Avatar =1,
        Photo,
        Document,
        Video,
        Audio
    }
}