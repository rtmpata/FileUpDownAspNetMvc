﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FileUpDownLoad.Models
{
    public class UploadDownloadContext :DbContext
    {

        public DbSet<File> Files { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<FilePath> FilePaths { get; set; }

        public UploadDownloadContext() : base("UploadDownload")
        {

        }
        public static UploadDownloadContext Create()
        {
            return new UploadDownloadContext();
        }
    }
}