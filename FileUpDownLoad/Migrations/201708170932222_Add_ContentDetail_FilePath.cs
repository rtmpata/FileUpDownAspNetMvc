namespace FileUpDownLoad.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ContentDetail_FilePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FilePaths", "ContentType", c => c.String());
            AddColumn("dbo.FilePaths", "ContentSize", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FilePaths", "ContentSize");
            DropColumn("dbo.FilePaths", "ContentType");
        }
    }
}
