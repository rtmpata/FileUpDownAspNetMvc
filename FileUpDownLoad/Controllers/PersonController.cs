﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FileUpDownLoad.Models;
using System.IO;
using System.Data.Entity.Infrastructure;

namespace FileUpDownLoad.Controllers
{
    public class PersonController : Controller
    {
        private UploadDownloadContext db = new UploadDownloadContext();

        // GET: Person
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }

        // GET: Person/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Include(p=>p.Files).Include(f=>f.FilePaths).FirstOrDefault(p=>p.ID == id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: Person/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Person/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LastName,FirstMidName")] Person person, 
            HttpPostedFileBase upload, HttpPostedFileBase uploadfs, HttpPostedFileBase[] files)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var uploadAvatar = upload;
                    var uploadPhoto = uploadfs;

                    // Avatar
                    if (uploadAvatar != null 
                        &&
                        uploadAvatar.ContentLength > 0 
                        && 
                        (uploadAvatar.ContentType.ToLower() == "image/jpeg" || uploadAvatar.ContentType.ToLower() == "image/png"))
                    {
                        var avatar = new FileUpDownLoad.Models.File
                        {
                            FileName = Path.GetFileName(upload.FileName),
                            FileType = FileType.Avatar,
                            ContentType = upload.ContentType
                        };
                        using (var reader = new BinaryReader(upload.InputStream))
                        {
                            avatar.Content = reader.ReadBytes(upload.ContentLength);
                        }
                        person.Files = new List<Models.File> { avatar };
                    }


                    // Photo
                    if (uploadPhoto != null
                        &&
                        uploadPhoto.ContentLength > 0
                        &&
                        (uploadPhoto.ContentType.ToLower() == "image/jpeg" || uploadPhoto.ContentType.ToLower() == "image/png"))
                    {
                        var guid = Guid.NewGuid().ToString();
                        var fileName = guid + Path.GetExtension(uploadPhoto.FileName);
                        var photo = new FilePath
                        {
                            //FileName = System.IO.Path.GetFileName(uploadPhoto.FileName),
                            /*FileName = Guid.NewGuid().ToString() + Path.GetExtension(uploadPhoto.FileName)*/
                            FileName = fileName,
                            FileType = FileType.Photo,
                        };
                        var path = Path.Combine(Server.MapPath("~/Content/Images"), fileName);
                        person.FilePaths = new List<FilePath>();
                        person.FilePaths.Add(photo);
                        uploadPhoto.SaveAs(path);
                    }

                    // Documents
                    if (files.Count() != 0)
                    {

                        //iterating through multiple file collection   
                        foreach (HttpPostedFileBase file in files)
                        {
                            //Checking file is available to save.  
                            if (file != null && file.ContentLength > 0)
                            {
                                string fileName = Path.GetFileName(file.FileName);
                                string fileType = file.ContentType;
                                int fileSize = file.ContentLength;
                                var document = new FilePath
                                {
                                    FileName = fileName,
                                    FileType = FileType.Document,
                                    ContentType = fileType,
                                    ContentSize = fileSize
                                };
                                
                                // giving a file system path
                                var path = Path.Combine(Server.MapPath("~/App_Data"), fileName);
                                //instantiating list of filepath
                                person.FilePaths = new List<FilePath>();
                                // Adding document in context
                                person.FilePaths.Add(document);
                                //Save file to server folder  
                                file.SaveAs(path);
                                ////assigning file uploaded status to ViewBag for showing message to user.  
                                //ViewBag.UploadStatus = files.Count().ToString() + " files uploaded successfully.";
                            }
                        }
                    }
                    db.People.Add(person);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            catch (RetryLimitExceededException /*dex*/)
            {
                // Log the error (uncomment dex varial name and add a line here to write a log)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
           

            return View(person);
        }

        // GET: Person/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Include(f => f.Files).SingleOrDefault(f => f.ID == id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Person/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LastName,FirstMidName")] Person person, HttpPostedFileBase upload, HttpPostedFileBase uploadfs)
        {
            if (ModelState.IsValid)
            {
                var personToUpdate = db.People.Find(person.ID);
                var uploadAvatar = upload;
                var uploadPhoto = uploadfs;
                try
                {

                    //Avatar
                    if (uploadAvatar != null
                        &&
                        uploadAvatar.ContentLength > 0
                        &&
                        (uploadAvatar.ContentType.ToLower() == "image/jpeg" || uploadAvatar.ContentType.ToLower() == "image/png"))
                    {
                        
                        if (personToUpdate.Files.Any(f => f.FileType == FileType.Avatar))
                        {
                            db.Files.Remove(personToUpdate.Files.First(f => f.FileType == FileType.Avatar));
                        }
                        var avatar = new Models.File
                        {
                            FileName = Path.GetFileName(uploadAvatar.FileName),
                            FileType = FileType.Avatar,
                            ContentType = uploadAvatar.ContentType
                        };
                        using (var reader = new BinaryReader(uploadAvatar.InputStream))
                        {
                            avatar.Content = reader.ReadBytes(uploadAvatar.ContentLength);
                        }
                        personToUpdate.Files = new List<Models.File> { avatar };
                    }


                    // Photo
                    //if (uploadPhoto != null && uploadPhoto.ContentLength > 0)
                    //{
                    //    var photo = new FilePath
                    //    {
                    //        FileName = System.IO.Path.GetFileName(uploadPhoto.FileName),
                    //        FileType = FileType.Photo
                    //    };
                    //    personToUpdate.FilePaths = new List<FilePath>();
                    //    personToUpdate.FilePaths.Add(photo);
                    //}
                }
                catch (RetryLimitExceededException /*dex*/)
                {
                    // Log the error
                    ModelState.AddModelError("", "Unable to save changes. Try again.");
                }
                db.Entry(personToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        #region way1
        //[HttpPost, ActionName("Edit")]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditPost(int? id, HttpPostedFileBase upload)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var studentToUpdate = db.People.Find(id);
        //    if (TryUpdateModel(studentToUpdate, "",
        //        new string[] { "LastName", "FirstMidName", "EnrollmentDate" }))
        //    {
        //        try
        //        {
        //            if (upload != null && upload.ContentLength > 0)
        //            {
        //                if (studentToUpdate.Files.Any(f => f.FileType == FileType.Avatar))
        //                {
        //                    db.Files.Remove(studentToUpdate.Files.First(f => f.FileType == FileType.Avatar));
        //                }
        //                var avatar = new Models.File
        //                {
        //                    FileName = System.IO.Path.GetFileName(upload.FileName),
        //                    FileType = FileType.Avatar,
        //                    ContentType = upload.ContentType
        //                };
        //                using (var reader = new System.IO.BinaryReader(upload.InputStream))
        //                {
        //                    avatar.Content = reader.ReadBytes(upload.ContentLength);
        //                }
        //                studentToUpdate.Files = new List<Models.File> { avatar };
        //            }
        //            db.Entry(studentToUpdate).State = EntityState.Modified;
        //            db.SaveChanges();

        //            return RedirectToAction("Index");
        //        }
        //        catch (RetryLimitExceededException /* dex */)
        //        {
        //            //Log the error (uncomment dex variable name and add a line here to write a log.
        //            ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
        //        }
        //    }
        //    return View(studentToUpdate);
        //}
        #endregion


        // GET: Person/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Person/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
