﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FileUpDownLoad.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var dir = new System.IO.DirectoryInfo(Server.MapPath("~/Content/Images/"));
            System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");
           
            List<string> items = new List<string>();
            foreach (var file in fileNames)
            {
                items.Add(file.Name);
            }

            return View(items);
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    
                    var fileType = file.ContentType;
                    if(fileType.ToLower() == "image/jpeg" || fileType.ToLower() == "image/png")
                    {
                        // giving a unique name to the file
                        var guid = Guid.NewGuid().ToString();
                        guid += (fileType.ToLower() == "image/jpeg") ? ".jpg" : ".png";
                        
                        // taking default file name
                        //var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/Images"), guid);
                        file.SaveAs(path);
                    }
                    else
                    {
                        ViewBag.Message = "Please choose image file";
                        return RedirectToAction("Upload");
                    }
                    
                }
                ViewBag.Message = "Upload successful";
                return RedirectToAction("Index");
                
            }
            catch
            {
                ViewBag.Message = "Upload failed";
                return RedirectToAction("Uploads");
            }
        }


        [HttpGet]
        public ActionResult Downloads()
        {
            var dir = new System.IO.DirectoryInfo(Server.MapPath("~/Content/Images/"));
            System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");

            List<string> items = new List<string>();
            foreach (var file in fileNames)
            {
                items.Add(file.Name);
            }

            return View(items);
        }

        public FileResult Download(string ImageName)
        {
            return File("~/Content/Images/" +ImageName, System.Net.Mime.MediaTypeNames.Application.Octet);
        }
    }
}